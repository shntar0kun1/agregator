FROM python:3.6-alpine
MAINTAINER Shntar0kun

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements/requirements.txt /requirements.txt
RUN apk add --no-cache --virtual .build-deps g++ gcc libxslt-dev python3-dev musl-dev postgresql-dev
RUN pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app
COPY . /app

RUN adduser -D user
USER user

