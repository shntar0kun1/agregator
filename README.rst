Агрегатор данных о погоде
=========================


Установка docker и docker-compose в Debian 9 Stretch
----------------------------------------------------
Обновите пакеты::

    $ apt-get update
    $ apt-get upgrade

Установите дополнительные пакеты::

    $ apt-get install apt-transport-https ca-certificates curl software-properties-common

Добавьте ключ GPG для хранилища Docker::

    $ wget https://download.docker.com/linux/debian/gpg
    $ sudo apt-key add gpg

Добавьте репозиторий Docker::

    $ echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee -a /etc/apt/sources.list.d/docker.list

Обновите базу пакетов и установите Docker и docker-compose::

    $ apt-get update
    $ apt-get install docker-ce docker-compose

Запустите и включите Docker для запуска при загрузке::

    $ systemctl start docker
    $ systemctl enable docker


Развертывание проекта
---------------------
Соберите Docker image::

    $ docker build .

Проведите миграции::

    $ docker-compose run web python /app/manage.py migrate --noinput --settings=agregator.settings.settings

Создайте админа сервиса::

    $ docker-compose run web python /app/manage.py createsuperuser --settings=agregator.settings.settings

Запустить тесты::

    $ docker-compose run web python /app/manage.py test agregator --settings=agregator.settings.settings

Запустите контейнер с проектом::

    $ docker-compose up -d --build


Документация
------------
Вся необходимая документация для работы с API будет доступна по ссылке на локалхосте:

`Документация <http://127.0.0.1:8000/api/>`_
