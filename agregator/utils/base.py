from typing import Iterable
import requests

from agregator.weather.models import City, Weather


class BaseParser:
    _URL_TEMPLATE = None
    _SOURCE_NAME = None

    @classmethod
    def _get_args(cls, city_obj):
        raise NotImplementedError("Class method `_get_args()` must be implemented")

    @classmethod
    def _make_request(cls, *args):
        url = cls._URL_TEMPLATE.format(*args[0])

        response = requests.get(url)
        response.raise_for_status()

        return response

    @classmethod
    def _extract_temperature(cls, response):
        raise NotImplementedError("Class method `_extract_data()` must be implemented")

    @classmethod
    def parse(cls, cities: Iterable[str]):
        for city_name in set(cities):
            city = City.objects.get(name=city_name)
            args = cls._get_args(city)
            response = cls._make_request(args)
            temperature = cls._extract_temperature(response)

            Weather.objects.create(
                city=city,
                source=cls._SOURCE_NAME,
                temperature=temperature
            )
