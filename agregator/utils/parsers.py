from django.conf import settings
from lxml import html

from agregator.weather.constants import YANDEX_WEATHER, OPEN_WEATHER_MAP
from .base import BaseParser


__all__ = [
    'YandexWeather',
    'OpenWeatherMap'
]


class YandexWeather(BaseParser):
    _URL_TEMPLATE = "https://yandex.ru/pogoda/city?lat={0}&lon={1}"
    _SOURCE_NAME = YANDEX_WEATHER

    @classmethod
    def _get_args(cls, city_obj):
        return city_obj.lat, city_obj.lon

    @classmethod
    def _extract_temperature(cls, response):
        tree = html.fromstring(response.content)
        temperature = tree.xpath(
            '//div[@class="fact__temp-wrap"]'
            '//div[@class="temp fact__temp fact__temp_size_s"]'
            '//span[@class="temp__value"]/text()'
        )

        return float(temperature[0])


class OpenWeatherMap(BaseParser):
    _URL_TEMPLATE = "https://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&appid={2}"
    _SOURCE_NAME = OPEN_WEATHER_MAP

    @classmethod
    def _get_args(cls, city_obj):
        return city_obj.lat, city_obj.lon, settings.OPEN_WEATHER_MAP_API_KEY

    @classmethod
    def _extract_temperature(cls, response):
        data = response.json()
        return float(data['main']['temp'] - 273)
