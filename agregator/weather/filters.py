from rest_framework.exceptions import NotFound
from django_filters.filters import Filter
from django_filters.rest_framework import FilterSet

from .models import Weather, City


class CityNameFilter(FilterSet):
    city_names = Filter(method='filter_by_city_names', label='City name(s)')

    class Meta:
        model = Weather
        fields = ()

    @staticmethod
    def filter_by_city_names(qs, name, value):
        """
        Does actual filtering by given cities.
        """
        cities = set(value.split(','))
        for city in cities:
            if not City.objects.filter(name=city).exists():
                raise NotFound(f"No city with name '{city}' in database")
        return qs.filter(city__name__in=cities)
