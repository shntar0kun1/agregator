from django.db.models import Max, Q
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.viewsets import GenericViewSet

from .filters import CityNameFilter
from .models import City, Weather
from .serializers import CitySerializer, WeatherSerializer
from ..utils import parsers


class CityViewSet(ListModelMixin, GenericViewSet):
    """
    Just for listing all available cities.
    """

    serializer_class = CitySerializer
    queryset = City.objects.all()


class WeatherViewSet(ListModelMixin, GenericViewSet):
    """
    ViewsSet for all weather data from DB.
    """

    serializer_class = WeatherSerializer
    queryset = Weather.objects.all()


class WeatherActualViewSet(WeatherViewSet):
    """
    Actual data about weather in parsed cities.
    Query parameter is a comma-separated string with city names.
    Example city_names=Tomsk,Moscow
    """

    filterset_class = CityNameFilter

    def get_queryset(self):
        values = Weather.objects.values('city__name', 'source').annotate(timestamp=Max('timestamp'))
        filter_list = Q()
        for item in values:
            filter_list |= Q(**item)

        return Weather.objects.filter(filter_list)

    @action(detail=False, methods=('get',))
    def refresh(self, request, *args, **kwargs):
        """
        Endpoint used to force data update from parsers.
        Query parameter is a comma-separated string with city names.
        Example city_names=Tomsk,Moscow
        """

        cities = request.query_params.get('city_names', None)
        try:
            if len(cities) == 0:
                raise Exception('No cities provided')
            cities = cities.split(',')
            all_parsers = [parsers.__dict__[key] for key in parsers.__all__]

            for parser in all_parsers:
                parser.parse(cities)
            return Response(status=HTTP_200_OK)
        except Exception as error:
            return Response(data={'detail': str(error)}, status=HTTP_500_INTERNAL_SERVER_ERROR)
