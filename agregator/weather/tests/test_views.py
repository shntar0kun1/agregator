from rest_framework import test, status
from rest_framework.reverse import reverse

from .mixins import LoginMixin
from ..constants import DATA_SOURCES


class TestCityViewSet(LoginMixin, test.APITestCase):
    client_class = test.APITestCase.client_class

    def setUp(self):
        super().setUp()
        self.cities_list_url = reverse('cities-list')

    def test_receive(self):
        self.login_as_user(self.admin_user)
        response = self.client.get(self.cities_list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_readonly(self):
        self.login_as_user(self.admin_user)
        response = self.client.post(
            self.cities_list_url,
            data={
                'name': 'Test',
                'lon': '63.456454',
                'lat': '63.456454'
            },
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestWeatherViewSet(LoginMixin, test.APITestCase):
    client_class = test.APITestCase.client_class

    def setUp(self):
        super().setUp()
        self.cities_list_url = reverse('weather-list')
        self.refresh_url = reverse('weather-refresh')

        self.accurate_data = {'city_names': 'Tomsk,Moscow'}
        self.inaccurate_data = {'city_names': 'Test1,Test2'}

    def test_refresh_existing_cities(self):
        self.login_as_user(self.admin_user)

        # Refreshing data for two cities
        response = self.client.get(self.refresh_url, data=self.accurate_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Checking data for cities
        response = self.client.get(self.cities_list_url, data=self.accurate_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], len(DATA_SOURCES)*2)

    def test_refresh_not_existing_cities(self):
        self.login_as_user(self.admin_user)

        response = self.client.get(self.refresh_url, data=self.inaccurate_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def test_only_recent_data_available(self):
        self.login_as_user(self.admin_user)

        # Refreshing data for two cities
        for i in range(0, 3):
            response = self.client.get(self.refresh_url, data=self.accurate_data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Checking data for cities
        response = self.client.get(self.cities_list_url, data=self.accurate_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], len(DATA_SOURCES)*2)

    def test_getting_data_for_not_existing_cities(self):
        self.test_only_recent_data_available()

        # Checking data for cities
        response = self.client.get(self.cities_list_url, data=self.inaccurate_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
