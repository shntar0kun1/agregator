from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token


class LoginMixin(object):
    """
    Mixin for more comfortable testing cases where login is required.
    Not needed now. Was written while it's all worked on token auth.
    """

    def setUp(self):
        model = get_user_model()
        self.admin_user = model.objects.create(
            username='adminuser',
            password='password',
            email='example@mail.com',
            is_superuser=True
        )
        token = Token.objects.create(user=self.admin_user)

    def login_as_user(self, user):
        self.client.logout()
        if hasattr(self.client, 'credentials'):
            self.client.credentials()
            self.client.credentials(
                HTTP_AUTHORIZATION='Token ' + user.auth_token.key
            )
        else:
            assert self.client.login(
                username=user.username,
                password='password',
            )