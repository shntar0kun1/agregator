from django.apps import AppConfig


class WeatherConfig(AppConfig):
    name = 'agregator.weather'
