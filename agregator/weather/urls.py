from rest_framework.routers import DefaultRouter

from .views import WeatherActualViewSet, CityViewSet, WeatherViewSet

router = DefaultRouter()
router.register(r'cities', CityViewSet, base_name='cities')
router.register(r'weather', WeatherActualViewSet, base_name='weather')
router.register(r'weatherhistory', WeatherViewSet, base_name='weather_history')
