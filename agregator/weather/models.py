from django.db import models

from .constants import DATA_SOURCES


class City(models.Model):
    name = models.CharField(max_length=150, unique=True)
    lon = models.DecimalField(max_digits=9, decimal_places=6)
    lat = models.DecimalField(max_digits=9, decimal_places=6)

    class Meta:
        unique_together = [
            ('lon', 'lat')
        ]


class Weather(models.Model):
    city = models.ForeignKey(to=City, on_delete=models.CASCADE)
    source = models.CharField(choices=DATA_SOURCES, max_length=6)
    temperature = models.FloatField()
    timestamp = models.DateTimeField(editable=False, auto_now=True)
