from rest_framework.serializers import ModelSerializer

from .models import City, Weather


class CitySerializer(ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class WeatherSerializer(ModelSerializer):
    city = CitySerializer(read_only=True)

    class Meta:
        model = Weather
        fields = '__all__'
