import os
import json

from django.db import migrations, transaction
from django.conf import settings


def fill_cities(apps, schema):
    City = apps.get_model('weather', 'City')
    data_file = os.path.join(settings.BASE_DIR, 'city.list.json')

    with open(data_file) as f:
        data = json.load(f)

    for d in data:
        if d['country'] == 'RU':
            try:
                with transaction.atomic():
                    City.objects.create(
                        name=d['name'],
                        lon=d['coord']['lon'],
                        lat=d['coord']['lat']
                    )
            except:
                pass


def backwards(apps, schema):
    pass


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('weather', '0001_initial')
    ]

    operations = [
        migrations.RunPython(fill_cities, reverse_code=backwards)
    ]
